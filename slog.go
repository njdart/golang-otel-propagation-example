package main

import (
	"context"
	"log/slog"

	"go.opentelemetry.io/otel/trace"
)

type otelHandler struct {
	slog.Handler
	recordingLevel slog.Level
}

func NewOtelSlogHandler(recorderLevel slog.Level, base slog.Handler) slog.Handler {
	return &otelHandler{
		Handler: base,
		recordingLevel: recorderLevel,
	}
}

// Enabled implements slog.Handler.
func (o *otelHandler) Enabled(ctx context.Context, l slog.Level) bool {
	span := trace.SpanFromContext(ctx)
	if span.IsRecording() {
		return l >= o.recordingLevel
	}
	return o.Handler.Enabled(ctx, l)
}

// Handle implements slog.Handler.
func (o *otelHandler) Handle(ctx context.Context, r slog.Record) error {
	span := trace.SpanFromContext(ctx)
	r.Add("isRecording", span.IsRecording())
	r.Add("traceId", span.SpanContext().TraceID().String())
	r.Add("spanId", span.SpanContext().SpanID().String())
	return o.Handler.Handle(ctx, r)
}

// WithAttrs implements slog.Handler.
func (o *otelHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return o.Handler.WithAttrs(attrs)
}

// WithGroup implements slog.Handler.
func (o *otelHandler) WithGroup(name string) slog.Handler {
	return o.Handler.WithGroup(name)
}

var _ slog.Handler = &otelHandler{}
