FROM golang:1.22.5-alpine

RUN go install github.com/cosmtrek/air@v1.49.0
WORKDIR /build
COPY go.mod go.sum ./
RUN go mod download
COPY . .

ENTRYPOINT ["air", "-c", ".air.toml"]