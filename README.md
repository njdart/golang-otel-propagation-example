# Go HTTP opentelemetry trace propagation example

An example project demonstrating trace propagation using W3C Propogators.

There are three "services" in this example, `serviceA`, `serviceB`, and `serviceC` - only serviceA is exposed to the
user at http://localhost:8080 but will make random calls to serviceB and serviceC and collect traces from all three.

Traces are sampled at 50%. However, with trace propagation, if a service decide to sample a trace, that decision is
indicated in the onward-request's headers and other servicse will respect that sampling decision (referred to as
`ParentBased` sampling in opentelemetry).

## Running

1. `docker-compose up --build`
1. open http://localhost:16686/ in your browser to view Jaeger
1. curl or open your browser to http://localhost:8080 to interact with the first service

## Examples sampled

```
$ curl localhost:8080
serviceA - IsRecording(true) TraceId(1897be530c1e7f5c686eca3dd221c091) SpanId(c304d95ba4d09e26)
serviceB - IsRecording(true) TraceId(1897be530c1e7f5c686eca3dd221c091) SpanId(1caa6500b3307fd8)
serviceC - IsRecording(true) TraceId(1897be530c1e7f5c686eca3dd221c091) SpanId(0225e844bc7fa713)
serviceC - IsRecording(true) TraceId(1897be530c1e7f5c686eca3dd221c091) SpanId(3eeabcc62fd71277)
serviceA - IsRecording(true) TraceId(1897be530c1e7f5c686eca3dd221c091) SpanId(370e83d66b61b213)
serviceC - IsRecording(true) TraceId(1897be530c1e7f5c686eca3dd221c091) SpanId(aa46d7a5b1c63e48)
```

![Example trace through multiple services](./example-jaeger.png)

## Example unsampled

```
$ curl localhost:8080
serviceA - IsRecording(false) TraceId(7d37417cf4d6d43ddc565480a9879b10) SpanId(715135663f4caf15)
serviceA - IsRecording(false) TraceId(7d37417cf4d6d43ddc565480a9879b10) SpanId(2005ccad6626bdcc)
serviceC - IsRecording(false) TraceId(7d37417cf4d6d43ddc565480a9879b10) SpanId(0808073423d50f8e)
serviceC - IsRecording(false) TraceId(7d37417cf4d6d43ddc565480a9879b10) SpanId(72fdfb7dc701bdf1)
serviceA - IsRecording(false) TraceId(7d37417cf4d6d43ddc565480a9879b10) SpanId(67b53b1286f4b918)
```
