package main

import (
	"context"
	"fmt"
	"io"
	"log/slog"
	"math/rand"
	"net/http"
	"os"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
	"go.opentelemetry.io/otel/trace"
)

var serviceName = ""
var log *slog.Logger

func handler(w http.ResponseWriter, r *http.Request) {
	// Span is initialised by otelhttp.NewHandler
	span := trace.SpanFromContext(r.Context())

	log.InfoContext(r.Context(), "Hello World", "SVC", serviceName)

	io.ReadAll(r.Body)

	w.WriteHeader(200)
	fmt.Fprintf(w,
		"%s - IsRecording(%v) TraceId(%s) SpanId(%s)\n",
		serviceName,
		span.IsRecording(),
		span.SpanContext().TraceID().String(),
		span.SpanContext().SpanID().String(),
	)

	// Choose a random service to call, including none
	url := ""
	switch rand.Intn(4) {
	case 0:
		url = "svc_a"
	case 1:
		url = "svc_b"
	case 2:
		url = "svc_c"
	default:
		log.ErrorContext(r.Context(), "Terminating trace")
		return
	}

	res, err := otelhttp.Get(r.Context(), "http://"+url+":8080/")
	if err != nil {
		fmt.Println(err)
		return
	}
	io.Copy(w, res.Body)
}

func init() {
	var ok bool
	serviceName, ok = os.LookupEnv("SERVICE_NAME")
	if !ok {
		panic("Expected SERVICE_NAME env to be set")
	}
}

func main() {
	initTraceProvider()
	log = slog.New(
		NewOtelSlogHandler(
			slog.LevelDebug,
			slog.NewJSONHandler(
				os.Stdout,
				&slog.HandlerOptions{Level: slog.LevelError},
			),
		),
	)

	http.Handle("/", otelhttp.NewHandler(
		http.HandlerFunc(handler),
		"GET /",
		// Add read and write events to the span
		otelhttp.WithMessageEvents(otelhttp.ReadEvents, otelhttp.WriteEvents),
	))

	http.ListenAndServe(":8080", nil)
}

func initTraceProvider() {
	// Set a global propogator, this is used in the otelhttp.NewHandler to fetch the `traceparent` header
	// and create a new trace with it. The `sdktrace.ParentBased` sampler will then be able to include
	// already-samples traces if they were sampled by an upstream service
	// See https://www.w3.org/TR/trace-context/
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	exporter, err := otlptrace.New(
		context.Background(),
		otlptracegrpc.NewClient(
			otlptracegrpc.WithInsecure(),
			otlptracegrpc.WithEndpoint("jaeger:4317"),
		),
	)
	if err != nil {
		panic(err)
	}
	r, err := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceName(serviceName),
		),
	)
	if err != nil {
		panic(err)
	}
	bsp := sdktrace.NewBatchSpanProcessor(exporter)
	tp := sdktrace.NewTracerProvider(
		// Sample 50% of requests, but downstream services will respect upstream sample rates
		sdktrace.WithSampler(sdktrace.ParentBased(sdktrace.TraceIDRatioBased(0.5))),
		sdktrace.WithSpanProcessor(bsp),
		sdktrace.WithResource(r),
	)
	otel.SetTracerProvider(tp)
}
